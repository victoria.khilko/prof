import pandas as pd

def prepare_dataset():

    # Извлекаем датасет из файлы json в датафрейм
    dataframe = pd.read_json('dataset/categories/new_categories/News_Category_Dataset_v3.json', lines=True)

    # Вывод всех категорий в датасете
    categories = set(dataframe['category'])
    print(*categories, sep='\n')
    # Вывод количества категорий в датасете
    print(len(categories))

    # Удаляем лишние столбцы
    dataframe = dataframe.drop(labels=['link', 'authors', 'date', 'headline'], axis=1)

    # Удаляем лишние строки
    category_for_delete = [
        'WORLD NEWS', 'WEDDINGS', 'DIVORCE', 'COMEDY', 'QUEER VOICES', 'WORLDPOST', 'THE WORLDPOST',
        'WOMEN', 'GOOD NEWS', 'FIFTY', 'WEIRD NEWS', 'BLACK VOICES', 'COLLEGE', 'PARENTS', 'IMPACT',
        'LATINO VOICES', 'RELIGION', 'MEDIA', 'GREEN', 'U.S. NEWS',
    ]
    for category in category_for_delete:
        dataframe = dataframe.drop(dataframe[dataframe['category'] == category].index)

    dataframe.to_json('new_dataset.json', orient='records')

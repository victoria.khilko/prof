import sqlite3

from keras.models import Sequential
from keras.layers import Embedding, LSTM, Dense, SpatialDropout1D
from keras.preprocessing import sequence
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
import pandas as pd
import spacy


# Загрузка модели SpaCy для английского языка
nlp = spacy.load("en_core_web_sm")

def clean_text(text):
    # Приведение текста к нижнему регистру
    text = text.lower()

    # Обработка текста с помощью SpaCy
    doc = nlp(text)

    # Лемматизация и удаление пунктуации
    lemmatized_words = [token.lemma_ for token in doc if not token.is_punct]

    # Соединение слов обратно в предложение
    normalized_text = " ".join(lemmatized_words)

    return normalized_text


def load_data_from_arrays(strings, labels, train_test_split=0.8):
    """ Подготовка обучающей и тестовой выборки. """
    data_size = len(strings)
    test_size = int(data_size - round(data_size * train_test_split))
    print("Test size: {}".format(test_size))

    print("\nTraining set:")
    x_train = strings[test_size:]
    print("\t - x_train: {}".format(len(x_train)))
    y_train = labels[test_size:]
    print("\t - y_train: {}".format(len(y_train)))

    print("\nTesting set:")
    x_test = strings[:test_size]
    print("\t - x_test: {}".format(len(x_test)))
    y_test = labels[:test_size]
    print("\t - y_test: {}".format(len(y_test)))

    return x_train, y_train, x_test, y_test


def prepare_dataset():

    # Извлекаем датасет из файлы json в датафрейм
    dataframe = pd.read_json('dataset/categories/new_categories/News_Category_Dataset_v3.json', lines=True)

    # Вывод всех категорий в датасете
    categories = set(dataframe['category'])
    print(*categories, sep='\n')
    # Вывод количества категорий в датасете
    print(len(categories))

    # Удаляем лишние столбцы
    dataframe = dataframe.drop(labels=['link', 'authors', 'date', 'headline'], axis=1)

    # Удаляем лишние строки
    category_for_delete = [
        'WORLD NEWS', 'WEDDINGS', 'DIVORCE', 'COMEDY', 'QUEER VOICES', 'WORLDPOST', 'THE WORLDPOST',
        'WOMEN', 'GOOD NEWS', 'FIFTY', 'WEIRD NEWS', 'BLACK VOICES', 'COLLEGE', 'PARENTS', 'IMPACT',
        'LATINO VOICES', 'RELIGION', 'MEDIA', 'GREEN', 'U.S. NEWS',
    ]
    for category in category_for_delete:
        dataframe = dataframe.drop(dataframe[dataframe['category'] == category].index)

    dataframe.to_json('new_dataset.json', orient='records')


def get_profession_by_category(category: str) -> str:
    with sqlite3.connect('professions.db') as conn:
        cur = conn.cursor()
        cur.execute("""SELECT * FROM professions WHERE category=?;""", (category,))
        profession = cur.fetchone()
        return profession


def make_lstm():
    # Извлекаем датасет из файла json в датафрейм
    dataframe = pd.read_json("new_dataset.json", orient="records")

    # Перемешиваем датасет
    dataframe = dataframe.sample(frac=1).reset_index(drop=True)

    # Печатаем датасет
    print(dataframe)

    # Вывод всех категорий в датасете
    categories = set(dataframe['category'])
    print(*categories, sep='\n')

    # Нормализация данных
    dataframe['short_description'] = dataframe.apply(lambda x: clean_text(x['short_description']), axis=1)
    print(dataframe)

    # Поставим в соответствие каждой категории номер
    categories = {}
    for key, value in enumerate(dataframe['category'].unique()):
        categories[value] = key + 1

    print(categories)

    # Запишем номер категории вместо названия
    dataframe['category'] = dataframe['category'].map(categories)
    print(dataframe)

    # Токенизация и преобразование в последовательности
    tokenizer = Tokenizer(num_words=5000, lower=True, oov_token='<OOV>')
    tokenizer.fit_on_texts(dataframe['short_description'])
    sequences = tokenizer.texts_to_sequences(dataframe['short_description'])
    padded_sequences = pad_sequences(sequences, maxlen=100)

    # Подготовка меток
    labels = dataframe['category'].values

    # Разделение данных на обучающую и тестовую выборки
    x_train, x_test, y_train, y_test = train_test_split(padded_sequences, labels, test_size=0.2, random_state=42)

    # Подготовка данных для модели LSTM
    vocab_size = len(tokenizer.word_index) + 1
    embedding_dim = 20
    max_length = 100
    num_classes = len(categories)

    # Создание и обучение модели LSTM
    model = Sequential()
    model.add(Embedding(input_dim=vocab_size, output_dim=embedding_dim, input_length=max_length))
    model.add(SpatialDropout1D(0.2))
    model.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(1, activation='softmax'))

    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

    # Тренировка модели
    epochs = 5
    batch_size = 64

    model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size, validation_data=(x_test, y_test), verbose=2)

    # Оценка модели
    loss, accuracy = model.evaluate(x_test, y_test, verbose=2)
    print(f'Accuracy: {accuracy}')

    return categories, model


def profession(categories, model):
    import pandas as pd
    history = pd.read_json("history.json")
    print(history)

    result = {}
    for value in categories.values():
        result[value] = 0

    count = 0
    for x in history.values:
        category = model.predict(str(x[0]))
        result[category] += 1
        count += 1

    for category, num in categories.items():
        if result[num] > 0:
            profession = get_profession_by_category(category)
            print(f'Категория {category}, '
                  f'количество: {round(result[num] / sum * 100, 2)}%. Подходящие профессии: {profession}')


if __name__ == '__main__':
    categories, model = make_lstm()
    profession(categories, model)
